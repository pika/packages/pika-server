FROM python:3.12.3-slim-bookworm as image_builder

# debian setup
RUN apt update && apt install -y \
    wget \
    curl \
    python3-dev \
    default-libmysqlclient-dev \
    build-essential \
    pkg-config \
    git

ARG SERVER_VERSION
# RUN wget https://gitlab.hrz.tu-chemnitz.de/pika/pika-server/-/archive/${SERVER_VERSION}/pika-server-${SERVER_VERSION}.tar.gz \
# && tar xzvf pika-server-${SERVER_VERSION}.tar.gz \
# && rm pika-server-${SERVER_VERSION}.tar.gz

RUN git clone --recursive --branch ${SERVER_VERSION} https://gitlab.hrz.tu-chemnitz.de/pika/pika-server.git pika-server-${SERVER_VERSION}

FROM image_builder as pika-server
RUN curl -sSL https://install.python-poetry.org | python3 -
ARG SERVER_VERSION
WORKDIR /pika-server-${SERVER_VERSION}/
COPY pika.conf pika.conf
COPY metric.json metric.json
COPY partition.json partition.json

ARG SERVER_MODE

RUN if [ "${SERVER_MODE}" = "dev" ]; then \
        ./development.sh --all; \
    else \
        ./production.sh --all; \
    fi

ENV MODE=${SERVER_MODE}
RUN chmod 744 src/pika_server.py
CMD $HOME/.local/bin/poetry run python src/pika_server.py ${MODE}

