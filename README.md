# pika-server

```sh
git clone https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-server.git
```

Modify `.env` according to your settings.

```
vi .env
SERVER_VERSION="main"
SERVER_PORT=4300
SERVER_MODE=prod
```

Copy the following example files and modify them according to your settings:

```
cp pika.conf.example pika.conf
cp metric.json.example metric.json
cp partition.json.example partition.json
```

Example for `pika.conf`:

```
[mariadb]
hostname       = "pika-mariadb" # ip address if not on the same host
port           = "3306"
database       = "pika"
#
user           = "readonly"
password       = "pika"
async_select_timeout_minutes = 5
#
admin_user     = "admin"
admin_password = "pika"

[influxdb-st]
hostname = "pika-influxdb" # ip address if not on the same host
port     = "8086"
user     = "readonly"
password = "pika"
database = "pika"

[influxdb-lt] # if not available, just use the same credentials as influxdb-st
hostname = "pika-influxdb" # ip address if not on the same host
port     = "8086"
user     = "readonly"
password = "pika"
database = "pika"

### REST API ###################################################################

[rest-api]
hostname             = "0.0.0.0"
port                 = 8000
proxy_hostname       = "" # only for testing, see README.md
proxy_user_key       = "REMOTE_USER"
cors_origin          = "http://localhost:4200" # replace with the corresponding ip address of pika-web
route_root_path      = "/"
...
```

**Hint**
PIKA requires a proxy server that handles user authentication, e.g. using Shibboleth.
The user name is sent to `pika-web` and `pika-server` via http header using the ProxyPass directive.

Example for apache configuration:

```
...
RequestHeader set REMOTE_USER %{REMOTE_USER}s

# backend
ProxyPass /backend/ http://pika-server:4300/
ProxyPassReverse /backend/ http://pika-server:4300/

# frontend
ProxyPass / http://pika-gui/
ProxyPassReverse / http://pika-gui/
```

For security reasons, the IP address of the proxy server must be entered in `pika.conf` under `proxy_hostname`.

For test purposes without a proxy server, the IP address in `proxy_hostname` can be omitted with an empty string.

If you want to install the proxy and `pika-server` on the same host, you need to specify `proxy_hostname` via the gateway ip address of the docker network `pika-net`:

```
docker network ls | grep pika-net > /dev/null || docker network create --driver bridge pika-net

docker network inspect pika-net
[
    {
        "Name": "pika-net",
        ...
            "Config": [
                {
                    "Subnet": "172.19.0.0/16",
                    "Gateway": "172.19.0.1"
                }
            ]
        },
...

vi pika.conf
...
proxy_hostname = "172.19.0.1"
```

**Hint:**
Our setup on ZIH uses a short-term and long-term database.
This setup enables us to carry out analyses over a longer period of time. At the moment this solution cannot be published yet, therefore the same InfluxDB credentials for `influxdb-st` and `influxdb-lt` should be used in `pika.conf`.

If you make changes to `pika.conf` after installation, please use `docker compose down` before calling `install.sh`.

```
./install.sh
```
