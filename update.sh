#!/bin/bash

# set new SERVER_VERSION in .env

docker compose down
docker system prune -a -f
docker volume prune -a -f

./install.sh
