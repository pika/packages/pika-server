#!/bin/bash

docker network ls | grep pika-net > /dev/null || docker network create --driver bridge pika-net
docker compose up -d --build

if [ $? -ne 0 ]; then
    echo "Error: Cannot install container."
    exit 1
fi
